# guestbook-rt


## Einführung

XAMPP ist notwendig, um das Installieren und Konfigurieren eines Apache Webservers mit einem Datenbanksystem und PHP-Skripten zu ermöglichen. 
Bitte installieren Sie XAMPP mit Administratorenrechten für den vollen Funktionsumfang. Sie finden es in diesem Repository unter `/Programmpakete`.

## Start

Nach dem Öffnen des installierten Programmpakets XAMPP werden folgende Server benötigt:
 - Apache
 - MySQL
Diese bitte nun auswählen und auf `Start` klicken.
Lokal laufen nun die beiden Server.

## Öffnen

Folgende lokale Verwaltungsmöglichkeiten haben Sie nun:
 - Die Willkommensseite von XAMPP mit allen [Hilfestellungen für MySQL/MariaDB, FTP, SQLite, Apache Tomcat, etc.](http://localhost/dashboard/).
 - Tool namens [phpMyAdmin](http://localhost/phpmyadmin/), um MySQL komfortabel über eine grafische Oberfläche zu verwalten.

Unter Ihrem Installationsverzeichnis von XAMPP (meist auf `C:\Programme\xampp` auffindbar) befindet sich der Ordner `htdocs`.
Alle Projektdateien, die Sie darin hinein schieben, können Sie nachfolgend direkt über einen Webbrowser Ihrer Wahl aufrufen (Firefox, Chrome, Edge, ..).
Hierzu müssen Sie nur folgende Adresse besuchen: [phpMyAdmin](http://localhost/).

## Bearbeiten

Bearbeiten Sie nun Ihr Webprojekt und lassen Sie es sich direkt durch ein Neuladen (F5) der Website im Browser anzeigen.

## Schließen

Nachdem Sie fertig sind, öffnen Sie das noch im Hintergrund laufende installierte Programmpaket-Fenster von XAMPP und beenden folgende folgende Server:
 - Apache
 - MySQL
Diese durch betätigen auf `Stopp` beenden.
Lokal sind nun alle Server von XAMPP beendet und Sie können das Fenster schließen.

## Fragen?

Im Falle einer Ungewissheit, schreiben Sie mich gerne an. Ich versuche, bei nächster Gelegenheit Ihnen Hilfestellung zu leisten. [mail@beispiel-reutlingen.de](mailto:mail@beispiel-reutlingen.de)

